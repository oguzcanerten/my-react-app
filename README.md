# My-React-App

In this repo, a CI-CD-cd pipeline created for a basic react app. When merged to main branch start the CI-CD pipeline.

<p>Application Link<p> 
<a href="http://hello-world-app-lb-2123453330.eu-central-1.elb.amazonaws.com/">http://hello-world-app-lb-2123453330.eu-central-1.elb.amazonaws.com/ </a>

<img src="./screenshots/app.png" width="500px" height="300px"
     alt="My App">
<hr>
<p>Diagram<p> 

<img src="./screenshots/drawio-diagram-oguzcan.png" width="500px" height="300px"
     alt="Diagram">
<hr>

## Technologies and Tools</h2>
- React.js
- Docker
-  Gitlab CI-CD Pipelines
- Terraform 
-  AWS
   - Amazon ECR
   - Elastic Container Service (Clusters, Task Definitions, Services) (Fargate)
   - IAM
   - Application Load Balancer for load balancing
 <hr>
 <h3>Dockerfile</h3>

- I tried to form an image with the smallest possible size. (23.71 MB)

<img src="./screenshots/my-dockerfile.png" width="500px" height="300px"
     alt="Dockerfile">

<hr>
 <h3>Pipeline Stages</h3>

 - I have two stages. I run tests while creating a docker image in build
  
 <img src="./screenshots/pipeline-stages.png" width="500px" height="300px"
     alt="stages">
<hr>
<h3>Terraform</h3>

- I created AWS ECS with Terraform (Clusters, Task Definitions, Services) (Fargate)
- Terraform implementations in Terraform folder
- terraform plan -out="tfplan"
- terraform apply "tfplan"

  <img src="./screenshots/terraform.png" width="500px" height="300px"
     alt="terraform">
<hr>
<h3>Load Balancer</h3>

  - For big size screenshots in screenshots folder
 
<img src="./screenshots/load-balancer-4.png" width="500px" height="300px"
     alt="terraform">

<img src="./screenshots/load-balancer-3.png" width="500px" height="300px"
     alt="terraform">

<img src="./screenshots/load-balancer-2.png" width="500px" height="300px"
     alt="terraform">

<img src="./screenshots/load-balancer-1.png" width="500px" height="300px"
     alt="terraform">

<hr>
<h3>Useful Resources For Me</h3>

 - https://docs.gitlab.com/ee/ci/cloud_deployment/
 - https://docs.gitlab.com/ee/ci/cloud_deployment/ecs/quick_start_guide.html#create-a-new-project-from-a-template
 




