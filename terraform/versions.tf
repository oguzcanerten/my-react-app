terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}
provider "aws" {
  region = <region>
  access_key = <access_key>
  secret_key = <secret_key>
}